package sk.miliczky.bigram;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BigramApplication {
	private static final String DEFAULT_FILE_NAME = "./input.txt";

	public static void main(String[] args) {
		BigramApplication app = new BigramApplication();
		app.run();
	}

	public void run() {
		// read file name
		String fileName = readFileNameFromInput();

		// load words from file
		List<String> words = loadWords(fileName);

		System.out.println("Processing words ...");
		// convert to list of bigrams
		List<Bigram> bigrams = convertToBigrams(words);

		// create histogram
		Map<Bigram, Integer> histogram = createHistogram(bigrams);

		// convert to set
		Set<Bigram> setOfBigrams = new LinkedHashSet<>(bigrams);
		System.out.println("Processing finished.");

		// print result
		printHistogram(setOfBigrams, histogram);
	}

	/**
	 * Reads filename from command line.
	 *
	 * @return read filename from command line
	 */
	private String readFileNameFromInput() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Type the name of the file, you'd like to load. [In case of empty file name, default will be used]");
		System.out.print("File name is: ");
		String fileName = sc.nextLine();
		if (fileName.equals("")) {
			fileName = DEFAULT_FILE_NAME;
		}
		sc.close();

		return fileName;
	}

	/**
	 * Loads list of word from provided file location/name.
	 *
	 * @param fileName file name
	 * @return list of words in that file
	 */
	private List<String> loadWords(String fileName) {
		List<String> words = new ArrayList<>();

		Scanner sc = null;
		BufferedReader br = null;
		System.out.println("Loading file '" + fileName + "' ... ");
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));
			sc = new Scanner(br);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				// preprocess the line - make it lower case
				line = line.toLowerCase();
				// preprocess the line - remove punctuation chars
				line = line.length() > 0 ? removePunctuationChars(line) : line;
				// populate list of words
				words.addAll(getWordsFromLine(line));
			}
			if (sc.ioException() != null) {
				throw sc.ioException();
			}
		} catch (IOException e) {
			System.out.println("Something went wrong when loading/reading file .. -> " + e.getMessage());
			System.exit(0);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("Something went wrong when closing file .. -> " + e.getMessage());
					System.exit(0);
				}
			}
			if (sc != null) {
				sc.close();
			}
		}
		System.out.println("Loading finished.");

		return words;
	}

	/**
	 * Removes punctuation characters from line of text.
	 * Should be one of !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~ chars. See https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html documentation for more details
	 *
	 * @param line of text
	 * @return line of text without punctuation chars.
	 */
	public String removePunctuationChars(String line) {
		return line.replaceAll("[\\p{Punct}\\p{IsPunctuation}]", "");
	}

	/**
	 * Gets list of words contained in one line of text.
	 *
	 * @param line of text
	 * @return list of words
	 */
	public List<String> getWordsFromLine(String line) {
		List<String> wordsInLine = new ArrayList<>();

		Pattern pattern = Pattern.compile("[a-zA-Z0-9]+");
		Matcher matcher = pattern.matcher(line);
		while (matcher.find()) {
			wordsInLine.add(matcher.group());
		}

		return wordsInLine;
	}

	/**
	 * Convert list of words to bigrams.
	 *
	 * @param words words
	 * @return list of bigrams
	 */
	public List<Bigram> convertToBigrams(List<String> words) {
		List<Bigram> result = new LinkedList<>();

		for (int i = 0; i < words.size() - 1; i++) {
			result.add(new Bigram(words.get(i), words.get(i + 1)));
		}

		return result;
	}

	/**
	 * Creates histogram based on list of bigrams
	 * @param bigrams bigrams
	 * @return histogram
	 */
	public Map<Bigram, Integer> createHistogram(List<Bigram> bigrams) {
		Map<Bigram, Integer> hist = new HashMap<>();

		for (Bigram aBigram : bigrams) {
			hist.merge(aBigram,1, Integer::sum);
		}

		return hist;
	}

	/**
	 * Prints bigrams and their counts to console. E.g.: "word1 word2" count,...
	 * @param bigrams set of bigrams
	 * @param histogram histogram
	 */
	private void printHistogram(Set<Bigram> bigrams, Map<Bigram, Integer> histogram) {
		System.out.println("Printing result ...");
		bigrams.forEach(bigram -> System.out.println(bigram + " " + histogram.get(bigram)));
	}
}

