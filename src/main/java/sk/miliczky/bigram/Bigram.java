package sk.miliczky.bigram;

public class Bigram {
    private final String word1;
    private final String word2;

    public Bigram(String word1, String word2) {
        this.word1 = word1;
        this.word2 = word2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bigram)) return false;

        Bigram bigram = (Bigram) o;

        if (!word1.equals(bigram.word1)) return false;
        return word2.equals(bigram.word2);
    }

    @Override
    public int hashCode() {
        int result = word1.hashCode();
        result = 31 * result + word2.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "\"" + word1 + " " + word2 + "\"";
    }
}
