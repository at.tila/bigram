package sk.miliczky.bigram;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConvertToBigramsTest extends BaseTest {
    @Test
    void testConvertToBigramsEmptyList() {
        List<String> words = new ArrayList<>();

        List<Bigram> bigrams = app.convertToBigrams(words);
        assertThat(0).isEqualTo(bigrams.size());
    }

    @Test
    void testConvertToBigramsEachDifferentWord() {
        List<String> words = new ArrayList<>();
        words.add("aaa");
        words.add("bbb");
        words.add("ccc");
        words.add("ddd");

        List<Bigram> bigrams = app.convertToBigrams(words);
        assertThat(words.size() - 1).isEqualTo(bigrams.size());
        assertThat(new Bigram("aaa", "bbb")).isEqualTo(bigrams.get(0));
        assertThat(new Bigram("bbb", "ccc")).isEqualTo(bigrams.get(1));
        assertThat(new Bigram("ccc", "ddd")).isEqualTo(bigrams.get(2));
    }

    @Test
    void testConvertToBigramsWithDuplicatePair() {
        List<String> words = new ArrayList<>();
        words.add("aaa");
        words.add("bbb");
        words.add("ccc");
        words.add("aaa");
        words.add("bbb");

        List<Bigram> bigrams = app.convertToBigrams(words);
        assertThat(words.size() - 1).isEqualTo(bigrams.size());
        assertThat(new Bigram("aaa", "bbb")).isEqualTo(bigrams.get(0));
        assertThat(new Bigram("bbb", "ccc")).isEqualTo(bigrams.get(1));
        assertThat(new Bigram("ccc", "aaa")).isEqualTo(bigrams.get(2));
        assertThat(new Bigram("aaa", "bbb")).isEqualTo(bigrams.get(3));
    }
}
