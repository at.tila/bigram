package sk.miliczky.bigram;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class WordsFromLineTest extends BaseTest {
    @Test
    void testGetWordsFromLineEmptyLine() {
        String line = "";

        List<String> words = app.getWordsFromLine(line);
        assertThat(0).isEqualTo(words.size());
    }

    @Test
    void testGetWordsFromLineNonEmptyLine() {
        String line = "aaa bbb ccc ddd eee";

        List<String> words = app.getWordsFromLine(line);
        assertThat(5).isEqualTo(words.size());
        assertThat("aaa").isEqualTo(words.get(0));
        assertThat("bbb").isEqualTo(words.get(1));
        assertThat("ccc").isEqualTo(words.get(2));
        assertThat("ddd").isEqualTo(words.get(3));
        assertThat("eee").isEqualTo(words.get(4));
    }

    @Test
    void testGetWordsFromLineNumberIncluded() {
        String line = "aaa bbb 111 ddd eee";

        List<String> words = app.getWordsFromLine(line);
        assertThat(5).isEqualTo(words.size());
        assertThat("aaa").isEqualTo(words.get(0));
        assertThat("bbb").isEqualTo(words.get(1));
        assertThat("111").isEqualTo(words.get(2));
        assertThat("ddd").isEqualTo(words.get(3));
        assertThat("eee").isEqualTo(words.get(4));
    }

    @Test
    void testGetWordsFromLineExtraSpaceBetween() {
        String line = "aaa   bbb c    ddd eee";

        List<String> words = app.getWordsFromLine(line);
        assertThat(5).isEqualTo(words.size());
        assertThat("aaa").isEqualTo(words.get(0));
        assertThat("bbb").isEqualTo(words.get(1));
        assertThat("c").isEqualTo(words.get(2));
        assertThat("ddd").isEqualTo(words.get(3));
        assertThat("eee").isEqualTo(words.get(4));
    }
}
