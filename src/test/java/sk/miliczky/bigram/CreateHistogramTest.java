package sk.miliczky.bigram;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class CreateHistogramTest extends BaseTest {
    @Test
    void testCreateHistogramEmpty() {
        List<Bigram> bigrams = new ArrayList<>();

        Map<Bigram, Integer> histogram = app.createHistogram(bigrams);
        assertThat(0).isEqualTo(histogram.size());
    }

    @Test
    void testCreateHistogramAtSize1() {
        List<Bigram> bigrams = new ArrayList<>();
        bigrams.add(new Bigram("qq", "cc"));

        Map<Bigram, Integer> histogram = app.createHistogram(bigrams);
        assertThat(1).isEqualTo(histogram.get(new Bigram("qq", "cc")));
    }

    @Test
    void testCreateHistogramDifferentBigrams() {
        List<Bigram> bigrams = new ArrayList<>();
        Bigram b1 = new Bigram("qq", "cc");
        Bigram b2 = new Bigram("cc", "qq");
        bigrams.add(b1);
        bigrams.add(b2);

        Map<Bigram, Integer> histogram = app.createHistogram(bigrams);
        assertThat(2).isEqualTo(histogram.size());
        assertThat(1).isEqualTo(histogram.get(b1));
        assertThat(1).isEqualTo(histogram.get(b2));
    }

    @Test
    void testCreateHistogramSameBigrams() {
        List<Bigram> bigrams = new ArrayList<>();
        Bigram b1 = new Bigram("qq", "cc");
        bigrams.add(b1);
        bigrams.add(b1);

        Map<Bigram, Integer> histogram = app.createHistogram(bigrams);
        assertThat(1).isEqualTo(histogram.size());
        assertThat(2).isEqualTo(histogram.get(b1));
    }

    @Test
    void testCreateHistogramSameBigramsMoreTimes() {
        List<Bigram> bigrams = new ArrayList<>();
        Bigram b1 = new Bigram("qq", "cc");
        Bigram b2 = new Bigram("cc", "qq");
        bigrams.add(b1);
        bigrams.add(b1);
        bigrams.add(b2);
        bigrams.add(b2);

        Map<Bigram, Integer> histogram = app.createHistogram(bigrams);
        assertThat(2).isEqualTo(histogram.size());
        assertThat(2).isEqualTo(histogram.get(b1));
        assertThat(2).isEqualTo(histogram.get(b2));
    }
}
