package sk.miliczky.bigram;

import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.BeforeEach;

public abstract class BaseTest implements WithAssertions {
    protected BigramApplication app;

    @BeforeEach
    void init() {
        app = new BigramApplication();
    }
}
