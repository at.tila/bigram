package sk.miliczky.bigram;

import org.junit.jupiter.api.Test;

import java.util.List;

public class RemovePunctuationCharsTest extends BaseTest {

    @Test
    void testRemovePunctuationCharsEmptyLine() {
        String line = "";

        String cleanedLine = app.removePunctuationChars(line);
        assertThat("").isEqualTo(cleanedLine);
    }

    @Test
    void testRemovePunctuationCharsNoPunctChar() {
        String line = "aa bb cc dd";

        String cleanedLine = app.removePunctuationChars(line);
        assertThat("aa bb cc dd").isEqualTo(cleanedLine);
    }

    @Test
    void testRemovePunctuationCharsOneInsideWord() {
        String line = "a.a bb cc dd";

        String cleanedLine = app.removePunctuationChars(line);
        assertThat("aa bb cc dd").isEqualTo(cleanedLine);
    }

    @Test
    void testRemovePunctuationCharsOneAfterWord() {
        String line = "aa, bb cc dd";

        String cleanedLine = app.removePunctuationChars(line);
        assertThat("aa bb cc dd").isEqualTo(cleanedLine);
    }

    @Test
    void testRemovePunctuationCharsAfterWordMoreOccurrences() {
        String line = "aa,?? bb cc dd";

        String cleanedLine = app.removePunctuationChars(line);
        assertThat("aa bb cc dd").isEqualTo(cleanedLine);
    }
}
